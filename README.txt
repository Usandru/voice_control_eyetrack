To run this project, get Unity version: 2018.2.11f1
Then clone the project into Unity.
Get the Tobii Unity SDK (https://github.com/Tobii/UnitySDK/releases) version 4.0.3 and import into the project.
Plug in a Tobii Eye Tracker 4C to your PC and run the calibration.
Enable Windows Speech functionality including Dictation support.
After this, the game may run.
To start the game, switch to the scene Main. Once started, press any key to start the game.

To run the analytics, go into PythonDataProcessing and run the Main.py script.