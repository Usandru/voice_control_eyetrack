﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;

public class GameManager : MonoBehaviour {

    //variables
    public static GameManager instance = null; //Static instance of GameManager which allows it to be accessed by any other script.
    public bool voice_level;
    bool second_level = false;
    bool voice_first = false;
    bool level_loaded = false;
    bool reverse_level_order = false;
    int load_num = 0;
    string scene_1 = "Level_1_Final";
    string scene_2 = "Level_2_Final";

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

    }

    private void Start()
    {
        if (!PhraseRecognitionSystem.isSupported)
        {
            TextIO.instance.WriteToLog("Windows Speech Recognition is not supported.");
            Application.Quit();
        }
    }

    public void LoadLevel()
    {
        if(OnLoadSetup.instance != null)
        {
            if (voice_level)
            {
                VoiceInputController.instance.Clear();
            }
        }
        if (!second_level)
        {
            if(OnLoadSetup.instance != null)
            {
                TextIO.instance.NewLog();
            }

            if (Random.value < 0.5f) reverse_level_order = true;
            else reverse_level_order = false;

            second_level = true;
            TextIO.instance.WriteToLog(reverse_level_order ? scene_1 : scene_2);

            if (Random.value < 0.5f)
            {
                voice_first = true;
                LoadVoice(reverse_level_order ? scene_1: scene_2);
            }
            else
            {
                LoadKeyboard(reverse_level_order ? scene_1 : scene_2);
            }
        }
        else
        {
            TextIO.instance.WriteToLog(reverse_level_order ? scene_2 : scene_1);
            second_level = false;
            if (voice_first)
            {
                LoadKeyboard(reverse_level_order ? scene_2 : scene_1);

            }
            else
            {
                LoadVoice(reverse_level_order ? scene_2 : scene_1);
            }
        }
    }

    private void LoadVoice(string scene)
    {
        TextIO.instance.WriteToLog("Loading Voice Level");
        TextIO.instance.NewEyeLog();
        voice_level = true;
        SceneManager.LoadScene(scene);
        load_num++;
        
    }

    private void LoadKeyboard(string scene)
    {
        TextIO.instance.WriteToLog("Loading Keyboard Level");
        voice_level = false;
        SceneManager.LoadScene(scene);
        load_num++;
    }

    private void Update()
    {
        if(Input.GetButton("Cancel") && Input.GetButton("Submit"))
        {
            TextIO.instance.EndLogging();
            Application.Quit();
        }

        if (Input.GetButton("Fire1") && Input.GetButton("Submit"))
        {
            TextIO.instance.WriteToLog("LEVEL LOAD OVERRIDE");
            LoadLevel();
        }

        if (level_loaded) return;

        if (Input.anyKeyDown)
        {
            level_loaded = true;
            LoadLevel();
        }
    }

}
