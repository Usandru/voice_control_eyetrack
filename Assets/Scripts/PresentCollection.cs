﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentCollection : MonoBehaviour {

	// Use this for initialization
	private void OnTriggerEnter(Collider col){
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();  // plays sound when collided.
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; item collected");
		OnLoadSetup.instance.pickup_counter++;
        OnLoadSetup.instance.CheckVictory();
        Destroy (gameObject);
	}

}
