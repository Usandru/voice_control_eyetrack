﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPositionController : MonoBehaviour {

    //variables
    [SerializeField]
    bool set_pos_numerically = false;
    [SerializeField]
    int x_pos;
    [SerializeField]
    int y_pos;
    [SerializeField]
    bool is_obstacle;
    [SerializeField]
    bool is_object;
    [SerializeField]
    bool is_goal;

	// Use this for initialization
	void Start () {

        Transform pos = gameObject.GetComponent<Transform>();
        Vector3 new_pos;

        if (!set_pos_numerically)
        {
            new_pos = new Vector3(Mathf.Round(pos.position.x), Mathf.Round(pos.position.y), pos.position.z);
        }
        else
        {
            new_pos = new Vector3(x_pos, y_pos, pos.position.z);
        }

        pos.position = new_pos;

        GameboardGridController.instance.SetSquare((int)pos.position.x, (int)pos.position.y, is_obstacle, is_object, is_goal);
	}
	
}
