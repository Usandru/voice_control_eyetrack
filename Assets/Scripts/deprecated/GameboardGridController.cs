﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameboardGridController : MonoBehaviour {

    //variables
    public static GameboardGridController instance = null; //Static instance of GameboardGridController which allows it to be accessed by any other script.
    [SerializeField]
    int x_board_size;
    [SerializeField]
    int y_board_size;
    GridSquareState[,] gameboard_squares;

    private struct GridSquareState
    {
        public bool obstructed;
        public bool contains_object;
        public bool goal_square;

        GridSquareState(bool obs, bool obj, bool goal)
        {
            obstructed = obs;
            contains_object = obj;
            goal_square = goal;
        }
    }

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        //creates the gameboard before any Start calls so the rest of the objects can populate it
        gameboard_squares = new GridSquareState[x_board_size, y_board_size];
    }

    public void SetSquare(int pos_x, int pos_y, bool obs = false, bool obj = false, bool goal = false)
    {
        gameboard_squares[pos_x, pos_y].obstructed = obs;
        gameboard_squares[pos_x, pos_y].contains_object = obj;
        gameboard_squares[pos_x, pos_y].goal_square = goal;
    }

}
