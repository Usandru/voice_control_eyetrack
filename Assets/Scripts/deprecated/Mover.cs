﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Mover : MonoBehaviour {

    [SerializeField]
    Transform targetPos;

    Seeker seeker;
    CharacterController controller;

    public Path path;

    public float speed = 2;
    public float nextWaypointDistance = 3;

    int currentWaypoint = 0;

    public bool reachedEndOfPath;

	// Use this for initialization
	void Start () {
        seeker = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();

        seeker.StartPath(transform.position, targetPos.position, OnPathComplete);
    }
	
	public void OnPathComplete(Path p)
    {
        Debug.Log("Path get " + p.error);

        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    public void Update()
    {
        if(path == null)
        {
            return;
        }

        reachedEndOfPath = false;

        float distanceToWaypoint;

        while (true)
        {
            distanceToWaypoint = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
            if(distanceToWaypoint < nextWaypointDistance)
            {
                if(currentWaypoint + 1 < path.vectorPath.Count)
                {
                    currentWaypoint++;
                }
                else
                {
                    reachedEndOfPath = true;
                    break;
                }
            }
            else
            {
                break;
            }
        }

        var speedFactor = reachedEndOfPath ? Mathf.Sqrt(distanceToWaypoint / nextWaypointDistance) : 1f;

        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        Vector3 velocity = dir * speed * speedFactor;

        controller.SimpleMove(velocity);
    }

}
