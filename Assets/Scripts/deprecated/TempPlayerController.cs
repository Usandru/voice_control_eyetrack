﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempPlayerController : MonoBehaviour {

    [SerializeField]
    MovementSystem self;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Up")) { self.Up(); }

        if (Input.GetButton("Down")) { self.Down(); }

        if (Input.GetButton("Left")) { self.Left(); }

        if (Input.GetButton("Right")) { self.Right(); }

        if (Input.GetButton("Go")) { self.Go(); }

        if (Input.GetButton("Stop")) { self.Stop(); }

    }
}
