﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public class EyeTracking : MonoBehaviour {

    [SerializeField]
    GazePlotter gaze;

    [SerializeField]
    ScreenPointToScene project;

    [SerializeField]
    InstructionListController instructions;

    public void GoToGaze()
    {
        GazePoint[] gaze_points = gaze.GetPoints();
        Vector2[] view_points = MapToView(gaze_points);

        Vector2 mid = SumVec2(view_points) / view_points.Length;

        ScreenPointToScene.GoodPoint p = project.GetRay(mid);

        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + p.state.ToString());

        if(p.state == ScreenPointToScene.ReportError.Success)
        {
            string xs = p.coord.x.ToString();
            string ys = p.coord.y.ToString();
            string zs = p.coord.z.ToString();
            string[] arr = { xs, ys, zs };

            instructions.Add_Context_Command("go there", string.Join(";", arr));
        }

    }

    private Vector2 SumVec2 (Vector2[] arr)
    {
        Vector2 v = Vector2.zero;

        foreach(Vector2 p in arr)
        {
            v = v + p;
        }

        return v;
    }

    private Vector2[] MapToView(GazePoint[] arr)
    {
        Vector2[] v_arr = new Vector2[arr.Length];

        for(int i = 0; i < arr.Length; i++)
        {
            v_arr[i] = arr[i].Viewport;
        }

        return v_arr;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
