﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showhidepanels : MonoBehaviour {

    public GameObject voicePanel;
    public GameObject movementPanel;

    int counter;

    public void showhide()
    {
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + "helptoggled");

        counter++;
        if (counter % 2 == 1)
        {
            voicePanel.gameObject.SetActive(false);
            movementPanel.gameObject.SetActive(false);
        }
        else
        {
            voicePanel.gameObject.SetActive(true);
            movementPanel.gameObject.SetActive(true);
        }

    }
}
