﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class PlayerMover : MonoBehaviour {

    Seeker seeker;
    CharacterController controller;

    Path path;

    float speed = 5;
    float nextWaypointDistance = 1f;

    int currentWaypoint = 0;

    bool reachedEndOfPath = true;
    bool calculating_path = false;

    Vector3 currentNode;

    // Use this for initialization
    void Start () {
        seeker = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();
        currentNode = transform.position;
        controller.SimpleMove(Vector3.right);
        //Debug.Log("state of path: " + reachedEndOfPath);
    }

    public bool PathState()
    {
        return reachedEndOfPath;
    }

    public bool IsCalculating()
    {
        return calculating_path;
    }

    public Vector3 CurrentNode()
    {
        return currentNode;
    }

    public void MoveTo(Vector3 position)
    {
        //do movement
        calculating_path = true;
        seeker.StartPath(currentNode, position, OnPathComplete);
    }

    void OnPathComplete(Path p)
    {
        Debug.Log("Path Error " + p.error);

        calculating_path = false;

        if (!p.error)
        {
            Debug.Log("on path complete called");
            path = p;
            currentWaypoint = 0;
            currentNode = path.vectorPath[path.path.Count - 1];
            reachedEndOfPath = false;
        }
    }

	// Update is called once per frame
	void Update ()
    {
        if (path == null)
        {
            return;
        }

        float distanceToWaypoint;

        while (true)
        {
            distanceToWaypoint = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
            if (distanceToWaypoint <= nextWaypointDistance)
            {
                if (currentWaypoint + 1 < path.vectorPath.Count)
                {
                    currentWaypoint++;
                }
                else
                {
                    reachedEndOfPath = true;
                    break;
                }
            }
            else
            {
                break;
            }
        }

        var speedFactor = reachedEndOfPath ? Mathf.Sqrt(distanceToWaypoint / nextWaypointDistance) : 1f;

        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        Vector3 velocity = dir * speed * speedFactor;

        controller.SimpleMove(velocity);
    }

    public void Reset()
    {
        reachedEndOfPath = true;
        calculating_path = false;
    }

}
