﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenPointToScene : MonoBehaviour {

    public enum ReportError { Success, Fail };

    public struct GoodPoint
    {
        public ReportError state;
        public Vector3 coord;

        public GoodPoint(ReportError err, Vector3 v)
        {
            state = err;
            coord = v;
        }
    }

    [SerializeField]
    Camera cam;
	
	// Update is called once per frame
	public GoodPoint GetRay (Vector2 view_pos) {
        RaycastHit hit;
        Ray ray = cam.ViewportPointToRay(view_pos);

        if(Physics.Raycast(ray, out hit))
        {
            return new GoodPoint(ReportError.Success, hit.point);
        }

        return new GoodPoint(ReportError.Fail, Vector3.one);
	}
}
