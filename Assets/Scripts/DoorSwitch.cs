﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour {

    public GameObject Door;
    bool doorIsOpening = false;
    public GameObject Switch;

    void OnTriggerEnter(Collider col)
    {
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; switch activated");
        Debug.Log("collision activated");
        if (doorIsOpening) return;
        doorIsOpening = true;
        Switch.transform.Translate(Vector3.down * 0.75f);
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();  // plays sound when collided.

    }
    private void Update()
    {

        if (doorIsOpening)
        {
            Door.transform.Translate(Vector3.up * Time.deltaTime * 5);

            if (Door.transform.position.y > 3f)
            {
                Door.GetComponent<Collider>().enabled = false;
                AstarPath.active.Scan();
                doorIsOpening = false;
                Destroy(Switch);
            }
        }





    }








}

