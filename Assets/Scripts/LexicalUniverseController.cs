﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LexicalUniverseController : MonoBehaviour {

    //variables
    public static LexicalUniverseController instance = null;              //Static instance of LexicalUniverseController which allows it to be accessed by any other script.
    Dictionary<string, HashSet<LexicalIdentity>> lexical_connections = new Dictionary<string, HashSet<LexicalIdentity>>();

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

    }

    //add an object and its lexical items to the lexical universe
    public void PopulateUniverse(List<string> lexical_items, LexicalIdentity object_identity)
    {
        foreach(string lex_itm in lexical_items)
        {
            //expand an existing set
            if (lexical_connections.ContainsKey(lex_itm))
            {
                lexical_connections[lex_itm].Add(object_identity);
            }
            //create a new set
            else
            {
                HashSet<LexicalIdentity> new_set = new HashSet<LexicalIdentity>();
                new_set.Add(object_identity);
                lexical_connections.Add(lex_itm, new_set);
            }
        }
    }

    //search the lexical universe for an object uniquely matching the search string
    public LexicalIdentity FindObject(List<string> search_string)
    {
        if (search_string == null) return null;
        //search through the lexical universe by doing set intersections of all sets given by the search strings
        HashSet<LexicalIdentity> search_set = null;
        foreach(string search_str in search_string)
        {
            if (!lexical_connections.ContainsKey(search_str)) continue;
            if (search_set == null) search_set = lexical_connections[search_str];
            else search_set.IntersectWith(lexical_connections[search_str]);
        }

        LexicalIdentity found_object = null;

        if (search_set == null) return null;
        //if there's only one remaining object reference set found_object to that value
        if (search_set.Count == 1)
        {
            foreach (LexicalIdentity identity in search_set)
            {
                found_object = identity;
            }
        }
        //we could add more complex behaviour to account for cases where the found objects are too many, 
        //but at the moment we're just returning null since that's easy.

        return found_object;
    }

}
