﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LexicalIdentity : MonoBehaviour {

    //variables. The "lexical identity" string will be split by spaces
    [SerializeField]
    string lexical_identity;
    LexicalUniverseController universe;

	// Use this for initialization
	void Start () {
        universe = LexicalUniverseController.instance;

        List<string> lex_list = new List<string>(lexical_identity.Split(' '));

        //add the lexical identity to the lexical universe
        universe.PopulateUniverse(lex_list, this);
	}
	
}
