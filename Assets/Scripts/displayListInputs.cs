﻿using UnityEngine;
using UnityEngine.UI;

public class displayListInputs : MonoBehaviour
{

    public InstructionListController instructionList;
    public Text displayInputList;

    // Update is called once per frame
    void Update()
    {
        string targetstring = string.Join("\n", instructionList.readList().ToArray());
        displayInputList.text = targetstring;
    }

}
